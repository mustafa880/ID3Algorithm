﻿
using System;
using System.Linq;
using System.Collections;
using System.Data;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace ID3Algorithm
{
    class Program
    {
        public static void printTree(TreeNode root, string tabs)
        {
            Console.WriteLine(tabs + '|' + root.attribute + '|');
            if (root.attribute.values != null)
            {
                for (int i = 0; i < root.attribute.values.Length; i++)
                {
                    TreeNode childNode = root.getChildByBranchName(root.attribute.values[i]);
                    if(childNode != null) { 
                    Console.WriteLine(tabs + "\t" + "<" + root.attribute.values[i] + ">");
                        printTree(childNode, "\t" + tabs);
                    }
                }
            }
        }

        static double[,] getExcelData()
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            Excel.Range range;

            double val;
            int rCnt;
            int cCnt;
            int rw = 0;
            int cl = 0;

            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Open(@"C:\Users\ok\Downloads\Desktop\Mustafa-Sites\DataSets\PCA.xlsx", 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            range = xlWorkSheet.UsedRange;
            rw = xlWorkSheet.Rows.CurrentRegion.EntireRow.Count;
            cl = xlWorkSheet.Columns.CurrentRegion.EntireColumn.Count;

            double[,] data = new double[rw-2,cl-2];

            for (rCnt = 3; rCnt <= rw; rCnt++)
            {
                for (cCnt = 3; cCnt <= cl; cCnt++)
                {
                    val = (double)(range.Cells[rCnt, cCnt] as Excel.Range).Value2;
                    data[rCnt-3, cCnt-3] = val;
                }
            }

            xlWorkBook.Close(true, null, null);
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);

            return data;
        }

        static string[] getExcelClasses()
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            Excel.Range range;

            string val;
            int rCnt;
            int rw = 0;

            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Open(@"C:\Users\ok\Downloads\Desktop\Mustafa-Sites\DataSets\PCA.xlsx", 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            range = xlWorkSheet.UsedRange;
            rw = xlWorkSheet.Rows.CurrentRegion.EntireRow.Count;
            string[] data = new string[rw - 2];

            for (rCnt = 3; rCnt <= rw; rCnt++)
            {
                    val = (string)(range.Cells[rCnt, 1] as Excel.Range).Value2;
                if (val == null)
                    val = data[rCnt - 4];
                    data[rCnt - 3] = val.Trim();
            }

            xlWorkBook.Close(true, null, null);
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);
            return data;
        }




        static Double findMax(Double[,] array, int column)
        {
            Double max = array[0, column];
            for (int i = 0; i < array.GetLength(0); i++)
            {
                if (array[i, column] > max)
                    max = array[i, column];
            }
            return max;
        }

        static Double findMin(Double[,] array, int column)
        {
            Double min = array[0, column];
            for (int i = 0; i < array.GetLength(0); i++)
            {
                if (array[i, column] < min)
                    min = array[i, column];
            }
            return min;
        }

        static DataTable getDatatableStructure()
        {
            String feat1 = "Deviation";
            String feat2 = "AreaProportion";
            String feat3 = "ShapeGeometricFactor";
            String feat4 = "Diameter";
            String feat5 = "Perimeter";
            String feat6 = "Area";
            String feat7 = "Eccentricity";
            String feat8 = "AspectRatio";
            DataTable result = new DataTable("samples");
            DataColumn column = result.Columns.Add(feat1);
            column.DataType = typeof(string);

            column = result.Columns.Add(feat2);
            column.DataType = typeof(string);

            column = result.Columns.Add(feat3);
            column.DataType = typeof(string);

            column = result.Columns.Add(feat4);
            column.DataType = typeof(string);

            column = result.Columns.Add(feat5);
            column.DataType = typeof(string);

            column = result.Columns.Add(feat6);
            column.DataType = typeof(string);

            column = result.Columns.Add(feat7);
            column.DataType = typeof(string);

            column = result.Columns.Add(feat8);
            column.DataType = typeof(string);

            column = result.Columns.Add("result");
            column.DataType = typeof(String);

            return result;
        }

        static DataTable getCategorical()
        {
            double[,] continousData = getExcelData();
            int k=3, n= continousData.GetLength(1); // K = Number of intervals, N = Number of attributes
            Double[] intervals = new Double[n];
            Double[,] boudaries = new Double[n, k+1];
            // Get Boundaries for each Attribute
            for (int i = 0; i < n; i++)
            {
                Double maxValue = findMax(continousData, i);
                Double minValue = findMin(continousData, i);
                boudaries[i, 0] = minValue;
                intervals[i] = (maxValue - minValue) / k;
                for (int z = 0; z < k; z++)
                    boudaries[i, z+1] = minValue + ((z + 1) * intervals[i]);
            }

            string feat8Abbreviation = "AR"; // Aspect ratio
            string feat7Abbreviation = "Ec";   // Eccentricity
            string feat6Abbreviation = "A";  //      Area   
            string feat5Abbreviation = "P";  //  Perimeter 
            string feat4Abbreviation = "Dia";  //    Diameter
            string feat3Abbreviation = "SGF";  //     Shape geometric factor
            string feat2Abbreviation = "AP";  //  Area proportion
            string feat1Abbreviation = "Dev";  //          Deviation

            DataTable CategoricalData = getDatatableStructure();
            string[] classes = getExcelClasses();
            for(int i= 0; i < continousData.GetLength(0); i++) {
                DataRow dr = CategoricalData.NewRow();
                dr[continousData.GetLength(1)] = classes[i];
                for(int j=0; j< continousData.GetLength(1); j++)
                {
                    switch (j)
                    {
                        case 0:
                            dr[j] = feat1Abbreviation + "1";
                            for (int m = boudaries.GetLength(1)-1; m > 0; m--)
                                if (continousData[i, j] <= boudaries[j,m] && continousData[i, j] >= boudaries[j, m-1])
                                    dr[j] = feat1Abbreviation + m;
                            break;
                        case 1:
                            dr[j] = feat2Abbreviation + "1";
                            for (int m = boudaries.GetLength(1) - 1; m > 0; m--)
                                if (continousData[i, j] <= boudaries[j, m] && continousData[i, j] >= boudaries[j, m - 1])
                                    dr[j] = feat2Abbreviation + m;
                            break;
                        case 2:
                            dr[j] = feat3Abbreviation + "1";
                            for (int m = boudaries.GetLength(1) - 1; m > 0; m--)
                                if (continousData[i, j] <= boudaries[j, m] && continousData[i, j] >= boudaries[j, m - 1])
                                    dr[j] = feat3Abbreviation + m;
                            break;
                        case 3:
                            dr[j] = feat4Abbreviation + "1";
                            for (int m = boudaries.GetLength(1) - 1; m > 0; m--)
                                if (continousData[i, j] <= boudaries[j, m] && continousData[i, j] >= boudaries[j, m - 1])
                                    dr[j] = feat4Abbreviation + m;
                            break;
                        case 4:
                            dr[j] = feat5Abbreviation + "1";
                            for (int m = boudaries.GetLength(1) - 1; m > 0; m--)
                                if (continousData[i, j] <= boudaries[j, m] && continousData[i, j] >= boudaries[j, m - 1])
                                    dr[j] = feat5Abbreviation + m;
                            break;
                        case 5:
                            dr[j] = feat6Abbreviation + "1";
                            for (int m = boudaries.GetLength(1) - 1; m > 0; m--)
                                if (continousData[i, j] <= boudaries[j, m] && continousData[i, j] >= boudaries[j, m - 1])
                                    dr[j] = feat6Abbreviation + m;
                            break;
                        case 6:
                            dr[j] = feat7Abbreviation + "1";
                            for (int m = boudaries.GetLength(1) - 1; m > 0; m--)
                                if (continousData[i, j] <= boudaries[j, m] && continousData[i, j] >= boudaries[j, m - 1])
                                    dr[j] = feat7Abbreviation + m;
                            break;
                        case 7:
                            dr[j] = feat8Abbreviation + "1";
                            for (int m = boudaries.GetLength(1) - 1; m > 0; m--)
                                if (continousData[i, j] <= boudaries[j, m] && continousData[i, j] >= boudaries[j, m - 1])
                                    dr[j] = feat8Abbreviation + m;
                            break;
                    }
                }
                // Add row
                CategoricalData.Rows.Add(dr);
            }
            // Remove Duplicate
            DataTable myData = getDistinct(CategoricalData);
            Console.WriteLine("Before Removing Irrelevant Rows");
            foreach (DataRow row in myData.Rows)
            {
                foreach (DataColumn column in myData.Columns)
                {
                    Console.Write(row[column] + "  ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine("After Removing Irrelevant Rows : Final Data");
            // TODO: Remove irrelevant 
            myData = removeIrrelevant(myData);
            foreach (DataRow row in myData.Rows)
            {
                foreach (DataColumn column in myData.Columns)
                {
                    Console.Write(row[column] + "  ");
                }
                Console.WriteLine();
            }

            return myData;
        }

        public static DataTable getDistinct(DataTable categoricalData)
        {
            string[] keys = new string[categoricalData.Columns.Count]; int i = 0;          
            foreach (DataColumn column in categoricalData.Columns)
            {
                keys[i] = column.ColumnName;
                i++;
            }
            return categoricalData.DefaultView.ToTable(true, keys);
        }

        public static DataTable removeIrrelevant(DataTable table1)
        {
            Boolean duplicate= false;
            DataTable categoricalData = getDatatableStructure();
            for(int i=0; i< table1.Rows.Count-1;i++)
            {
                var array1 = table1.Rows[i].ItemArray;
                var array1Edit = array1.Take(array1.Count() - 1).ToArray();
                for (int j= i+1; j<table1.Rows.Count; j++)
                {
                    var array2 = table1.Rows[j].ItemArray;
                    array2 = array2.Take(array2.Count() - 1).ToArray();
                     duplicate = false;
                    if (array1Edit.SequenceEqual(array2))
                    {
                        duplicate = true;
                        break;
                    }
                }
                if(!duplicate)
                    categoricalData.Rows.Add(array1);
            }
            return categoricalData;
        }

        
        static void Main(string[] args)
        {                                        
            int origWidth = Console.WindowWidth;
            int origHeight = Console.WindowHeight;
            Console.SetWindowSize(origWidth, origHeight+15);


            String feat1 = "Deviation";
            String feat2 = "AreaProportion";
            String feat3 = "ShapeGeometricFactor";
            String feat4 = "Diameter";
            String feat5 = "Perimeter";
            String feat6 = "Area";
            String feat7 = "Eccentricity";
            String feat8 = "AspectRatio";

            DataTable samples = getCategorical();

            string[] feats = new string[] { feat1, feat2, feat3, feat4, feat5, feat6, feat7, feat8 };

            Attribute Deviation = null, AreaProportion = null, ShapeGeometricFactor = null, Diameter = null, Perimeter = null, Area = null, Eccentricity=null, AspectRatio=null;
            for(int i=1; i<=8; i++) {
                string location = feats[i-1];
            var x = (from r in samples.AsEnumerable()
                     select (string)r[location]).Distinct().ToArray();
                switch (i)
                {
                    case 1:
                          Deviation = new Attribute(feat1, x);
                        break;
                    case 2:
                         AreaProportion = new Attribute(feat2, x);
                        break;
                    case 3:
                         ShapeGeometricFactor = new Attribute(feat3, x);
                        break;
                    case 4:
                         Diameter = new Attribute(feat4, x);
                        break;
                    case 5:
                         Perimeter = new Attribute(feat5, x);
                        break;
                    case 6:
                        Area = new Attribute(feat6, x);
                        break;
                    case 7:
                        Eccentricity = new Attribute(feat7, x);
                        break;
                    case 8:
                        AspectRatio = new Attribute(feat8, x);
                        break;
                }
            }
            Attribute[] attributes = new Attribute[] { Deviation, AreaProportion, ShapeGeometricFactor, Diameter, Perimeter, Area, Eccentricity, AspectRatio };

            DecisionTreeID3 id3 = new DecisionTreeID3();
            TreeNode root = id3.generateMyTree(samples, "result", attributes);
            printTree(root, "");
            Console.ReadKey();

        }
    }

    /// <summary>
    /// Class representing an attribute used in the class of decision
    /// </summary>
    public class Attribute
    {
        ArrayList mValues;
        string mName;
        object mLabel;

        /// <summary>
        /// Initializes a new instance of an Atribute class
        /// </summary>
        /// <param name="name">Indicates attribute name</param>
        /// <param name="values">Indicates the possible values for the attribute</param>
        public Attribute(string name, string[] values)
        {
            mName = name;
            mValues = new ArrayList(values);
            mValues.Sort();
        }

        public Attribute(object Label)
        {
            mLabel = Label;
            mName = string.Empty;
            mValues = null;
        }

        /// <summary>
        /// Indicates attribute name
        /// </summary>
        public string AttributeName
        {
            get
            {
                return mName;
            }
        }

        /// <summary>
        /// Returns an array with attribute values
        /// </summary>
        public string[] values
        {
            get
            {
                if (mValues != null)
                    return (string[])mValues.ToArray(typeof(string));
                else
                    return null;
            }
        }

        /// <summary>
        /// Returns the index of a value
        /// </summary>
        /// <param name="value">Value to Return</param>
        /// <returns>The value of the index in which the value position is</returns>
        public int indexValue(string value)
        {
            if (mValues != null)
                return mValues.BinarySearch(value);
            else
                return -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (mName != string.Empty)
            {
                return mName;
            }
            else
            {
                return mLabel.ToString();
            }
        }
    }

    /// <summary>
    /// Class representing the assembled decision tree;
    /// </summary>
    public class TreeNode
    {
        private ArrayList mChilds = null;
        private Attribute mAttribute;

        /// <summary>
        /// Initializes a new instance of TreeNode
        /// </summary>
        /// <param name="attribute">Attribute to which the node is connected</param>
        public TreeNode(Attribute attribute)
        {
            if (attribute.values != null)
            {
                mChilds = new ArrayList(attribute.values.Length);
                for (int i = 0; i < attribute.values.Length; i++)
                    mChilds.Add(null);
            }
            else
            {
                mChilds = new ArrayList(1);
                mChilds.Add(null);
            }
            mAttribute = attribute;
        }

        /// <summary>
        /// Adds a child TreeNode to this treenode in the named branch named ValueName
        /// </summary>
        /// <param name="treeNode">TreeNode child to be added</param>
        /// <param name="ValueName">Name of the branch where treeNode is created</param>
        public void AddTreeNode(TreeNode treeNode, string ValueName)
        {
            int index = mAttribute.indexValue(ValueName);
            mChilds[index] = treeNode;
        }

        /// <summary>
        /// Returns the total number of children in n
        /// </summary>
        public int totalChilds
        {
            get
            {
                return mChilds.Count;
            }
        }

        /// <summary>
        /// Returns the n child of an n
        /// </summary>
        /// <param name="index">Index of the child</param>
        /// <returns>An object of the TreeNode class representing n</returns>
        public TreeNode getChild(int index)
        {
            return (TreeNode)mChilds[index];
        }

        /// <summary>
        /// Attribute that is connected to the N
        /// </summary>
        public Attribute attribute
        {
            get
            {
                return mAttribute;
            }
        }

        /// <summary>
        /// Returns the child of an n by the name of the branch that leads to it
        /// </summary>
        /// <param name="branchName">Twig Name</param>
        /// <returns>an n</returns>
        public TreeNode getChildByBranchName(string branchName)
        {
            int index = mAttribute.indexValue(branchName);
            return (TreeNode)mChilds[index];
        }
    }

    /// <summary>
    /// Class that implements a Decision Tree using the ID3 algorithm
    /// </summary>
    public class DecisionTreeID3
    {
        private DataTable mSamples;       
        private String classElliptocytosis = "Elliptocytosis";
        private String classNormal = "Normal";
        private String classTearDrop = "Tear drop";
        private String classThalassemia = "Thalassemia";
        private String classIron = "Iron";
        private String classSickle = "Sickle";
        private String classTarget = "Target";

        private int mTotalElliptocytosis = 0;
        private int mTotalNormal = 0;
        private int mTotalTearDrop = 0;
        private int mTotalThalassemia = 0;
        private int mTotalIron = 0;
        private int mTotalSickle = 0;
        private int mTotalTarget = 0;

        private int mTotal = 0;
        private string mTargetAttribute = "result";
        private double mEntropySet = 0.0;              

        
        private int countTotalElliptocytosis(DataTable samples)
        {
            int result = 0;

            foreach (DataRow aRow in samples.Rows)
            {
                if (((String)aRow[mTargetAttribute]).Trim() == classElliptocytosis)
                    result++;
            }

            return result;
        }

        private int countTotalNormal(DataTable samples)
        {
            int result = 0;

            foreach (DataRow aRow in samples.Rows)
            {
                if (((String)aRow[mTargetAttribute]).Trim() == classNormal)
                    result++;
            }

            return result;
        }

        private int countTotalTearDrop(DataTable samples)
        {
            int result = 0;

            foreach (DataRow aRow in samples.Rows)
            {
                if (((String)aRow[mTargetAttribute]).Trim() == classTearDrop)
                    result++;
            }

            return result;
        }

        private int countTotalThalassemia(DataTable samples)
        {
            int result = 0;

            foreach (DataRow aRow in samples.Rows)
            {
                if (((String)aRow[mTargetAttribute]).Trim() == classThalassemia)
                    result++;
            }

            return result;
        }

        private int countTotalIron(DataTable samples)
        {
            int result = 0;

            foreach (DataRow aRow in samples.Rows)
            {
                if (((String)aRow[mTargetAttribute]).Trim() == classIron)
                    result++;
            }

            return result;
        }

        private int countTotalSickle(DataTable samples)
        {
            int result = 0;

            foreach (DataRow aRow in samples.Rows)
            {
                if (((String)aRow[mTargetAttribute]).Trim() == classSickle)
                    result++;
            }

            return result;
        }

        private int countTotalTarget(DataTable samples)
        {
            int result = 0;

            foreach (DataRow aRow in samples.Rows)
            {
                if (((String)aRow[mTargetAttribute]).Trim() == classTarget)
                    result++;
            }

            return result;
        }

        private double calculateEntropy(int elliptocytosis, int normal, int tearDrop, int thalassemia, int iron, int sickle, int target)
        {
            int total = elliptocytosis + normal + tearDrop + thalassemia + iron + sickle + target;
            double ratioElliptocytosis = 0;
            double ratioNormal = 0;
            double ratioTearDrop = 0;
            double ratioThalassemia = 0;
            double ratioIron = 0;
            double ratioSickle = 0;
            double ratioTarget = 0;

            if (total != 0)
            {
                ratioElliptocytosis = (double)elliptocytosis / total;
                ratioNormal = (double)normal / total;
                ratioTearDrop = (double)tearDrop / total;
                ratioThalassemia = (double)thalassemia / total;
                ratioIron = (double)iron / total;
                ratioSickle = (double)sickle / total;
                ratioTarget = (double)target / total;
            }


            if (ratioElliptocytosis != 0)
                ratioElliptocytosis = -(ratioElliptocytosis) * System.Math.Log(ratioElliptocytosis, 2);
            if (ratioNormal != 0)
                ratioNormal = -(ratioNormal) * System.Math.Log(ratioNormal, 2);
            if (ratioTearDrop != 0)
                ratioTearDrop = -(ratioTearDrop) * System.Math.Log(ratioTearDrop, 2);
            if (ratioThalassemia != 0)
                ratioThalassemia = -(ratioThalassemia) * System.Math.Log(ratioThalassemia, 2);
            if (ratioIron != 0)
                ratioIron = -(ratioIron) * System.Math.Log(ratioIron, 2);
            if (ratioSickle != 0)
                ratioSickle = -(ratioSickle) * System.Math.Log(ratioSickle, 2);
            if (ratioTarget != 0)
                ratioTarget = -(ratioTarget) * System.Math.Log(ratioTarget, 2);

            double result = ratioElliptocytosis + ratioNormal + ratioTearDrop + ratioThalassemia + ratioIron + ratioSickle + ratioTarget;
            return result;
        }




        
        private void getValuesByAttribute(DataTable samples, Attribute attribute, string value,out int elliptocytosis, out int normal, out int tearDrop, out int thalassemia, out int iron, out int sickle, out int target)
        {
            elliptocytosis = 0;
            normal = 0;
            tearDrop = 0;
            thalassemia = 0;
            iron = 0;
            sickle = 0;
            target = 0;

            foreach (DataRow aRow in samples.Rows)
            {
                if ((((string)aRow[attribute.AttributeName]).Trim() == value))
                    if (((String)aRow[mTargetAttribute]).Trim() == classElliptocytosis)
                        elliptocytosis++;
                    else if (((String)aRow[mTargetAttribute]).Trim() == classNormal)
                        normal++;
                    else if (((String)aRow[mTargetAttribute]).Trim() == classTearDrop)
                        tearDrop++;
                    else if (((String)aRow[mTargetAttribute]).Trim() == classThalassemia)
                        thalassemia++;
                    else if (((String)aRow[mTargetAttribute]).Trim() == classIron)
                        iron++;
                    else if (((String)aRow[mTargetAttribute]).Trim() == classSickle)
                        sickle++;
                    else if (((String)aRow[mTargetAttribute]).Trim() == classTarget)
                        target++;
            }
        }


      
        private double calculateGain(DataTable samples, Attribute attribute)
        {
            string[] values = attribute.values;
            double sum = 0.0;

            for (int i = 0; i < values.Length; i++)
            {
                int elliptocytosis, normal, tearDrop, thalassemia, iron, sickle, target;

                elliptocytosis = normal = tearDrop = thalassemia = iron = sickle = target = 0;

                getValuesByAttribute(samples, attribute, values[i], out elliptocytosis, out normal, out tearDrop, out thalassemia, out iron, out sickle, out target);

                double entropy = calculateEntropy(elliptocytosis, normal, tearDrop, thalassemia, iron, sickle, target);
                sum += -(double)(elliptocytosis + normal + tearDrop + thalassemia + iron + sickle + target) / mTotal * entropy;
            }
            return mEntropySet + sum;
        }



        
        private Attribute getBestAttr(DataTable samples, Attribute[] attributes)
        {
            double maxGain = 0.0;
            Attribute result = null;

            foreach (Attribute attribute in attributes)
            {
                double aux = calculateGain(samples, attribute);
                if (aux >= maxGain)
                {
                    maxGain = aux;
                    result = attribute;
                }
            }
            return result;
        }
        
        private bool allSamplesElliptocytosis(DataTable samples, string targetAttribute)
        {
            foreach (DataRow row in samples.Rows)
            {
                if (((String)row[targetAttribute]).Trim() != classElliptocytosis)
                    return false;
            }

            return true;
        }

        private bool allSamplesNormal(DataTable samples, string targetAttribute)
        {
            foreach (DataRow row in samples.Rows)
            {
                if (((String)row[targetAttribute]).Trim() != classNormal)
                    return false;
            }

            return true;
        }

        private bool allSamplesTearDrop(DataTable samples, string targetAttribute)
        {
            foreach (DataRow row in samples.Rows)
            {
                if (((String)row[targetAttribute]).Trim() != classTearDrop)
                    return false;
            }

            return true;
        }

        private bool allSamplesThalassemia(DataTable samples, string targetAttribute)
        {
            foreach (DataRow row in samples.Rows)
            {
                if (((String)row[targetAttribute]).Trim() != classThalassemia)
                    return false;
            }

            return true;
        }

        private bool allSamplesIron(DataTable samples, string targetAttribute)
        {
            foreach (DataRow row in samples.Rows)
            {
                if (((String)row[targetAttribute]).Trim() != classIron)
                    return false;
            }

            return true;
        }

        private bool allSamplesSickle(DataTable samples, string targetAttribute)
        {
            foreach (DataRow row in samples.Rows)
            {
                if (((String)row[targetAttribute]).Trim() != classSickle)
                    return false;
            }

            return true;
        }

        private bool allSamplesTarget(DataTable samples, string targetAttribute)
        {
            foreach (DataRow row in samples.Rows)
            {
                if (((String)row[targetAttribute]).Trim() != classTarget)
                    return false;
            }

            return true;
        }






        
        /// <summary>
        /// Returns a list with all the different values of a sample table
        /// </summary>
        /// <param name="samples">DataTable with Samples</param>
        /// <param name="targetAttribute">Attribute (column) of the table to be checked</param>
        /// <returns>An ArrayList with different values</returns>
		private ArrayList getDistinctValues(DataTable samples, string targetAttribute)
        {
            ArrayList distinctValues = new ArrayList(samples.Rows.Count);

            foreach (DataRow row in samples.Rows)
            {
                if (distinctValues.IndexOf(row[targetAttribute]) == -1)
                    distinctValues.Add(row[targetAttribute]);
            }

            return distinctValues;
        }

        /// <summary>
        /// Returns the most common value within a sample
        /// </summary>
        /// <param name="samples">DataTable with Samples</param>
        /// <param name="targetAttribute">Attribute (column) of the table to be checked</param>
        /// <returns>Returns the object with the highest incidence within the sample table</returns>
        private object getMostCommonValue(DataTable samples, string targetAttribute)
        {
            ArrayList distinctValues = getDistinctValues(samples, targetAttribute);
            int[] count = new int[distinctValues.Count];

            foreach (DataRow row in samples.Rows)
            {
                int index = distinctValues.IndexOf(row[targetAttribute]);
                count[index]++;
            }

            int MaxIndex = 0;
            int MaxCount = 0;

            for (int i = 0; i < count.Length; i++)
            {
                if (count[i] > MaxCount)
                {
                    MaxCount = count[i];
                    MaxIndex = i;
                }
            }

            return distinctValues[MaxIndex];
        }

        private TreeNode generateInternalTree(DataTable samples, string targetAttribute, Attribute[] attributes)
        {
            if (allSamplesElliptocytosis(samples, targetAttribute) == true)
                return new TreeNode(new Attribute(classElliptocytosis));

            if (allSamplesNormal(samples, targetAttribute) == true)
                return new TreeNode(new Attribute(classNormal));

            if (allSamplesTearDrop(samples, targetAttribute) == true)
                return new TreeNode(new Attribute(classTearDrop));

            if (allSamplesThalassemia(samples, targetAttribute) == true)
                return new TreeNode(new Attribute(classThalassemia));

            if (allSamplesIron(samples, targetAttribute) == true)
                return new TreeNode(new Attribute(classIron));

            if (allSamplesSickle(samples, targetAttribute) == true)
                return new TreeNode(new Attribute(classSickle));

            if (allSamplesTarget(samples, targetAttribute) == true)
                return new TreeNode(new Attribute(classTarget));


            if (attributes.Length == 0)
                return new TreeNode(new Attribute(getMostCommonValue(samples, targetAttribute)));

            mTotal = samples.Rows.Count;
            mTargetAttribute = targetAttribute;

            mTotalElliptocytosis = countTotalElliptocytosis(samples);
            mTotalNormal = countTotalNormal(samples);
            mTotalTearDrop = countTotalTearDrop(samples);
            mTotalThalassemia = countTotalThalassemia(samples);
            mTotalIron = countTotalIron(samples);
            mTotalSickle = countTotalSickle(samples);
            mTotalTarget = countTotalTarget(samples);



            mEntropySet = calculateEntropy(mTotalElliptocytosis, mTotalNormal, mTotalTearDrop,mTotalThalassemia, mTotalIron, mTotalSickle, mTotalTarget);

            Attribute bestAttribute = getBestAttr(samples, attributes);
            TreeNode root = new TreeNode(bestAttribute);

            DataTable aSample = samples.Clone();

            foreach (string value in bestAttribute.values)
            {
                // Selects all elements with the value of this attribute				
                aSample.Rows.Clear();

                DataRow[] rows = samples.Select(bestAttribute.AttributeName + " = " + "'" + value + "'");

                if (rows.Length == 0)
                    continue;

                foreach (DataRow row in rows)
                {
                    aSample.Rows.Add(row.ItemArray);
                }

                // Creates a new list of attributes minus the current attribute that is the best attribute				
                ArrayList aAttributes = new ArrayList(attributes.Length - 1);
                for (int i = 0; i < attributes.Length; i++)
                {
                    if (attributes[i].AttributeName != bestAttribute.AttributeName)
                        aAttributes.Add(attributes[i]);
                }

                if (aSample.Rows.Count == 0)
                {
                    return new TreeNode(new Attribute(getMostCommonValue(samples, targetAttribute)));
                }
                else
                {
                    DecisionTreeID3 dc3 = new DecisionTreeID3();
                    TreeNode ChildNode = dc3.generateMyTree(aSample, targetAttribute, (Attribute[])aAttributes.ToArray(typeof(Attribute)));
                    root.AddTreeNode(ChildNode, value);
                }
            }

            return root;
        }

        
        public TreeNode generateMyTree(DataTable samples, string targetAttribute, Attribute[] attributes)
        {
            mSamples = samples;
            return generateInternalTree(mSamples, targetAttribute, attributes);
        }
        
    }

}

